package academia.negocio;

import academia.interfaces.InterfaceAutenticavel;
import academia.pessoa.Aluno;

public final class AcessoAluno implements InterfaceAutenticavel<Object> {
    
    private Aluno aluno;
    
    @Override
    public boolean autenticavel(String id, Object biometria) {
        return true;
    }
    
    public void setAlunoAcesso(Aluno a) {
        aluno = a;
    }
    
    public Antropometria getRelatorioAntropometrico() {
        return aluno.getAntropometria();
    }
    
    public boolean getStatusPagamento() {
        return aluno.isPagamento();
    }
}
