package academia.negocio;

import academia.interfaces.InterfaceAutenticavel;
import academia.modalidade.Modalidade;
import academia.pessoa.Administrador;
import academia.pessoa.Aluno;
import java.util.List;

public final class AcessoGestor implements InterfaceAutenticavel<String> {

    private String lista;
    
    public List<Aluno> alunosPorModalidade(Modalidade modalidade) {
        return modalidade.getModalidadeAlunos();
    }
    
    public String[] getListaModalidades(List<Modalidade> m) {
        lista = "";
        
        for (Modalidade modalidade : m) {
            lista += "" + modalidade.getCodigo() + " - " + modalidade.getNome() + ",";
        }
        return lista.split(",");
    }
   
    public String[] getListaAlunosStatus(List<Aluno> a, boolean pago) {
        lista = "";
        
        for (Aluno aluno : a) {
            if (aluno.isPagamento() == pago) {
                lista += aluno.getMatricula() + " - " + aluno.getNome() + " - " + aluno.getCpf() + ",";
            }      
        }       
        return lista.split(",");
    }

    public String[] getListaAlunos(List<Aluno> a) {
        lista = "";
        
        for (Aluno aluno : a) {
            lista += aluno.getMatricula() + " - " + aluno.getNome() + " - " + aluno.getCpf() + ",";
        }       
        return lista.split(",");
    }
    
    public String[] getListaAdministradores(List<Administrador> a) {
        lista = "";
        
        for (Administrador administrador : a) {
            lista += administrador.getMatricula() + " - " + administrador.getNome() + " - " + administrador.getCpf() + ",";
        }
        return lista.split(",");
    }

    public String[] getListaAlunosModalidade(Modalidade modalidade) {
        
        lista = "";
 
        List<Aluno> alunos = alunosPorModalidade(modalidade);
        
        for (Aluno aluno : alunos) {
            lista += aluno.getMatricula() + " - " + aluno.getNome() + " - " + aluno.getCpf() + ",";
        }
        
        return lista.split(",");
    }

    @Override
    public boolean autenticavel(String id, String identificacao) {
        return true;
    }
}