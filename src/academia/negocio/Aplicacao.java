package academia.negocio;

import academia.cruds.CrudAluno;
import academia.cruds.CrudAlunoModalidade;
import academia.cruds.CrudModalidade;
import academia.modalidade.Modalidade;
import academia.pessoa.Administrador;
import academia.pessoa.Aluno;

public class Aplicacao {

    // Declarações dos tipos
    private Aluno aluno;
    private Administrador administrador;
    private Modalidade modalidade;
    
    // CRUDs
    private CrudAluno crudAluno = new CrudAluno();
    private CrudModalidade crudModalidade = new CrudModalidade();
    private CrudAlunoModalidade crudAlunoModalidade = new CrudAlunoModalidade();
    
       
    public void adicionarAluno(String nome, String cpf, String endereco, char sexo, int idade) {
        aluno = new Aluno(nome, cpf, endereco, sexo, idade);
        crudAluno.inserir(aluno);
    }
    
    public void adicionarModalidade(String nome, int codigo) {
        modalidade = new Modalidade(nome, codigo);
        crudModalidade.inserir(modalidade);
    }
    
    public void adicionarAlunoModalidade(Aluno aluno, Modalidade modalidade) {
         crudAlunoModalidade.inserir(modalidade, aluno);
    }
    
    public void alterarAluno(Aluno aluno, String nome, String cpf,String endereco, char sexo, int idade) {
        crudAluno.alterar(aluno, nome, cpf,endereco, sexo, idade);
    }
    
    public void alterarModalidade(Modalidade modalidade, String nome, int codigo) {
        crudModalidade.alterar(modalidade, nome, codigo);
    }    
}
