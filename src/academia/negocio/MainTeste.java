package academia.negocio;

import academia.pessoa.Aluno;
import academia.cruds.CrudAluno;
import academia.cruds.CrudAlunoModalidade;
import academia.modalidade.Modalidade;
import java.util.ArrayList;

public class MainTeste {
    
    public static void imprimir(String msg) {
        System.out.println(msg);
    }
    
    public static void main(String... args) {
        Modalidade m = new Modalidade("Natação", 001);
        CrudAlunoModalidade cam = new CrudAlunoModalidade();
        
        Aluno a = new Aluno("Jose", "09052515477", "Rua X", 'M', 20);
        Aluno b = new Aluno("Fernando", "08052515417", "Rua B", 'M', 20);
        Aluno c = new Aluno("Maria", "09052515474", "Rua A", 'M', 20);
        
        AcessoGestor gestor = new AcessoGestor();
        CrudAluno ca = new CrudAluno();
        
        ca.inserir(a);
        ca.inserir(b);
        
        ca.deletar(a);
        
        String[] aLista = gestor.getListaAlunos(ca.getListaAlunos());
        for (String string : aLista) {
            System.out.println(string);
        }
        
        System.out.println("---1");
        
        ca.inserir(c);
        
        
        
        
        cam.inserir(m, a);
        cam.inserir(m, b);

        //imprimir(m.getModalidadeAlunos().toString());
               
        //imprimir(m.toString());

        // Pesquisa por Fernado
       // imprimir("Consulta: " + ca.consultar("Fernando"));
        
        
        //gestor.imprimirAlunosModalidade(m);
        
        a.setPagamento(true);
        
        
        //gestor.imprimirAlunos(ca.getListaAlunos());
        
    }
}
