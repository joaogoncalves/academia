package academia.negocio;

public final class Antropometria {
    
    private Double altura;
    private Double massa;
    private Double bicips;
    private Double torax;

    public Antropometria() {
    }
    
    public Antropometria(Double altura, Double massa, Double bicips, Double torax) {
        this.altura = altura;
        this.massa = massa;
        this.bicips = bicips;
        this.torax = torax;
    }
    
    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public Double getPeso() {
        return massa;
    }

    public void setPeso(Double peso) {
        this.massa = peso;
    }

    public Double getImc() {
        return massa / Math.pow(altura, 2);
    }

    public Double getBicips() {
        return bicips;
    }

    public void setBicips(Double bicips) {
        this.bicips = bicips;
    }

    public Double getTorax() {
        return torax;
    }

    public void setTorax(Double torax) {
        this.torax = torax;
    }
}
