package academia.pessoa;

public class Administrador extends Pessoa {

    public Administrador(String nome) {
        setNome(nome);
    }

    public Administrador(String nome, String cpf) {
        setNome(nome);
        setCpf(cpf);
    }

    @Override
    public String toString() {
        return getNome();
    }
}
