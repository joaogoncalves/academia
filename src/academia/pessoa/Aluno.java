package academia.pessoa;

import academia.negocio.Antropometria;

public class Aluno extends Pessoa {
    
    private String endereco;
    private char sexo;
    private int idade;
    private Antropometria antropometria;
    private boolean pagamento;
    
    /**
     * Construtor padrão
     */
    public Aluno() {
    }

    public Aluno(String nome, String cpf, String endereco, char sexo, int idade) {
        setNome(nome);
        setCpf(cpf);
        this.endereco = endereco;
        this.sexo = sexo;
        this.idade = idade;
        this.pagamento = false;
    }
    
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public Antropometria getAntropometria() {
        return antropometria;
    }

    public void setAntropometria(Antropometria antropometria) {
        this.antropometria = antropometria;
    }

    public boolean isPagamento() {
        return pagamento;
    }

    public void setPagamento(boolean pagamento) {
        this.pagamento = pagamento;
    }
    
    @Override
    public String toString() {
        return "" + getMatricula() + " - " + getNome() + " - " +  getCpf();
    }
}