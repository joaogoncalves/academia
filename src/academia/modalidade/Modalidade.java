package academia.modalidade;

import academia.pessoa.Aluno;
import java.util.ArrayList;
import java.util.List;

public class Modalidade {
    
    private String nome;
    private int codigo;
    private ArrayList<Aluno> modalidadeAlunos;

    public Modalidade() {
        this.modalidadeAlunos = new ArrayList<>();
    }

    public Modalidade(String nome, int codigo) {
        this.nome = nome;
        this.codigo = codigo;
        this.modalidadeAlunos = new ArrayList<>();
    }   
    
    public void adicionar(Aluno aluno) {
        this.modalidadeAlunos.add(aluno);
    }
    
    public void remover(Aluno aluno) {
        modalidadeAlunos.remove(aluno);
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<Aluno> getModalidadeAlunos() {
        return modalidadeAlunos;
    }

    public void setModalidadeAlunos(ArrayList<Aluno> modalidadeAlunos) {
        this.modalidadeAlunos = modalidadeAlunos;
    }

    @Override
    public String toString() {
        return "" + codigo + " - " + nome;
    }
}