package academia.gui;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class JIFRelatorioAlunosStatusPagamento extends javax.swing.JInternalFrame {

    public JIFRelatorioAlunosStatusPagamento() {
        initComponents();

        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        
        buttonGroupFiltragem.add(radioButtonTodos);
        buttonGroupFiltragem.add(radioButtonPagos);
        buttonGroupFiltragem.add(radioButtonDebitos);
        
    }
    
    public void botaoSelecionadoListar() {
        String[] lista = {};

        if (radioButtonTodos.isSelected()) {
            lista = JFAplicacao.getListaAlunos();
            adicionaListagem(lista);
        } else if (radioButtonPagos.isSelected()) {
            lista = JFAplicacao.getListaAlunosStatus(true);
            adicionaListagem(lista);
        } else if (radioButtonDebitos.isSelected()) {
            lista = JFAplicacao.getListaAlunosStatus(false);
            adicionaListagem(lista);
        } else {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma opção de filtragem!", "Aviso", 2);
        }

    }
    
    public void adicionaListagem(final String[] lista) {
        jList1.setModel(new javax.swing.AbstractListModel() {
            @Override
            public int getSize() {
                return lista.length;
            }

            @Override
            public Object getElementAt(int i) {
                return lista[i];
            }
        });
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2,
                (d.height - this.getSize().height) / 2);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupFiltragem = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel2 = new javax.swing.JPanel();
        radioButtonTodos = new javax.swing.JRadioButton();
        radioButtonPagos = new javax.swing.JRadioButton();
        radioButtonDebitos = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Relatório de Pagamentos");
        setPreferredSize(new java.awt.Dimension(640, 480));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista dos Clientes"));

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Selecione um filtro" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jList1);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtragem"));

        radioButtonTodos.setText("Todos");

        radioButtonPagos.setText("Pagos");

        radioButtonDebitos.setText("Débitos");

        jButton1.setText("Filtrar");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(radioButtonTodos)
                .addGap(74, 74, 74)
                .addComponent(radioButtonPagos)
                .addGap(88, 88, 88)
                .addComponent(radioButtonDebitos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(59, 59, 59))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioButtonTodos)
                    .addComponent(radioButtonPagos)
                    .addComponent(radioButtonDebitos)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("empty-statement")
    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        botaoSelecionadoListar();
    }//GEN-LAST:event_jButton1MouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupFiltragem;
    private javax.swing.JButton jButton1;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioButtonDebitos;
    private javax.swing.JRadioButton radioButtonPagos;
    private javax.swing.JRadioButton radioButtonTodos;
    // End of variables declaration//GEN-END:variables
}
