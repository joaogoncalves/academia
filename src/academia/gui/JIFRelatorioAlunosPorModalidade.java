package academia.gui;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public final class JIFRelatorioAlunosPorModalidade extends javax.swing.JInternalFrame {

    public JIFRelatorioAlunosPorModalidade() {
        initComponents();

        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        
    }
    
    public void adicionarListagemModalidades(String[] lista) {
        comboModalidades.setModel(
                new javax.swing.DefaultComboBoxModel(lista));
    }
    
    public void adicionaListagemAlunos(final String[] lista) {
        listAlunos.setModel(new javax.swing.AbstractListModel() {
            @Override
            public int getSize() {
                return lista.length;
            }

            @Override
            public Object getElementAt(int i) {
                return lista[i];
            }
        });
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2,
                (d.height - this.getSize().height) / 2);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupFiltragem = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        listAlunos = new javax.swing.JList();
        jPanel2 = new javax.swing.JPanel();
        buttonSelecionar = new javax.swing.JButton();
        comboModalidades = new javax.swing.JComboBox();

        setClosable(true);
        setIconifiable(true);
        setTitle("Relatório de Pagamentos");
        setPreferredSize(new java.awt.Dimension(640, 480));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista dos Clientes"));

        listAlunos.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Selecione um filtro" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listAlunos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(listAlunos);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Modalidades"));

        buttonSelecionar.setText("Selecionar");
        buttonSelecionar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonSelecionarMouseClicked(evt);
            }
        });

        comboModalidades.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(comboModalidades, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(buttonSelecionar)
                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSelecionar)
                    .addComponent(comboModalidades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("empty-statement")
    private void buttonSelecionarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonSelecionarMouseClicked
        String nomeModalidade = comboModalidades.getSelectedItem().toString();
        String[] listaAlunos = JFAplicacao.getListaAlunosModalidade(nomeModalidade);
        
        adicionaListagemAlunos(listaAlunos);
    }//GEN-LAST:event_buttonSelecionarMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupFiltragem;
    private javax.swing.JButton buttonSelecionar;
    private javax.swing.JComboBox comboModalidades;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList listAlunos;
    // End of variables declaration//GEN-END:variables
}
