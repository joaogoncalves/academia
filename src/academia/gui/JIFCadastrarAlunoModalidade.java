package academia.gui;

import academia.modalidade.Modalidade;
import academia.pessoa.Aluno;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class JIFCadastrarAlunoModalidade extends javax.swing.JInternalFrame {
    
    private String tipo;
    private static Aluno aluno;
    private static Modalidade modalidade;
    private static String selecionadoAluno = "";
    private static String selecionadoModalidade = "";
    private static boolean ativarEscolhaExclusao = false;
    
    public JIFCadastrarAlunoModalidade() {
        initComponents();
        
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
    }
    
    private void cadastrarAlunoModalidade() {
        try {
            selecionadoAluno = listAlunos.getSelectedValue().toString();
            selecionadoModalidade = listModalidade.getSelectedValue().toString();

            if ( !(selecionadoAluno.equals("")) || (!selecionadoModalidade.equals(""))) {
                aluno = JFAplicacao.pesquisarAluno(selecionadoAluno);
                modalidade = JFAplicacao.pesquisaModalidade(selecionadoModalidade);
                System.out.println(modalidade.toString());
                
                JFAplicacao.cadastrarAlunoModalidade(modalidade, aluno);
                
                JOptionPane.showMessageDialog(rootPane, "Aluno Associado a Modalidade!", "Salvo", 1);
            }
            
        } catch (RuntimeException re) {
            JOptionPane.showMessageDialog(rootPane, "\nSelecione uma modalidade e \n"
                    + "um aluno a serem associados!", "Aviso", 2);
        }
    }
    
    private void excluirAlunoModalidade() {
        try {
            selecionadoAluno = listAlunos.getSelectedValue().toString();
            
            if ( !(selecionadoAluno.equals("")) || (!selecionadoModalidade.equals(""))) {
                aluno = JFAplicacao.pesquisarAluno(selecionadoAluno);
                modalidade = JFAplicacao.pesquisaModalidade(selecionadoModalidade);
                
                JFAplicacao.deletarAlunoModalidade(modalidade, aluno);
                
                JOptionPane.showMessageDialog(rootPane, "Aluno Desassociado da Modalidade!", "Exclusão", 1);
                
                listagemSelecionadoExclusao();
            }
            
        } catch (RuntimeException re) {
            JOptionPane.showMessageDialog(rootPane, "Não há um aluno selecionado ou não há alunos a serem selecionados!", "Aviso", 2);
        }
    }
    
    public void setListagemCadastrar(String[] itensModalidade, String[] itensAlunos) {
        adicionarListaModalidades(itensModalidade);
        adicionarListaAlunos(itensAlunos);
    }
    
    public void setListagemExcluir(String[] itensModalidade) {
        adicionarListaModalidades(itensModalidade);
        buttonAssociar.setText("Desassociar");
        setTitle("Desassociar Aluno da Modalidade");
        ativarEscolhaExclusao = true;
    }
    
    public void adicionarListaAlunos(final String[] lista) {
        listAlunos.setModel(new javax.swing.AbstractListModel() {
            @Override
            public int getSize() {
                return lista.length;
            }

            @Override
            public Object getElementAt(int i) {
                return lista[i];
            }
        });
    }
    
    public void adicionarListaModalidades(final String[] lista) {
        listModalidade.setModel(new javax.swing.AbstractListModel() {
            @Override
            public int getSize() {
                return lista.length;
            }

            @Override
            public Object getElementAt(int i) {
                return lista[i];
            }
        });
    }
    
    public void listagemSelecionadoExclusao() {
        try {
                selecionadoModalidade = listModalidade.getSelectedValue().toString();
                modalidade = JFAplicacao.pesquisaModalidade(selecionadoModalidade);
                String[] itensNomes = JFAplicacao.pesquisarAlunoModalidade(modalidade);
                
                adicionarListaAlunos(itensNomes);
                
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Erro ao escolher modalidade para exclusão!", "Aviso", 2);
            }
        }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2,
                (d.height - this.getSize().height) / 2);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listAlunos = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        listModalidade = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        buttonAssociar = new javax.swing.JButton();
        buttonCancelar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Associar Aluno em Molidade");

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Listagem de Alunos"));

        listAlunos.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Vazio" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listAlunos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(listAlunos);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("Listagem de Modalidades"));

        listModalidade.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Vazio" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listModalidade.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listModalidade.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listModalidadeMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(listModalidade);

        buttonAssociar.setText("Associar");
        buttonAssociar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonAssociarMouseClicked(evt);
            }
        });

        buttonCancelar.setText("Cancelar");
        buttonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonCancelarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonAssociar)
                .addGap(18, 18, 18)
                .addComponent(buttonCancelar)
                .addGap(21, 21, 21))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonAssociar)
                    .addComponent(buttonCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAssociarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonAssociarMouseClicked
        if (!ativarEscolhaExclusao) {
            cadastrarAlunoModalidade();
        } else {
            excluirAlunoModalidade();
        }
        
    }//GEN-LAST:event_buttonAssociarMouseClicked

    private void buttonCancelarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonCancelarMouseClicked
        selecionadoAluno = "";
        selecionadoModalidade = "";
        this.dispose();
    }//GEN-LAST:event_buttonCancelarMouseClicked

    private void listModalidadeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listModalidadeMouseClicked
        if (ativarEscolhaExclusao) {
            listagemSelecionadoExclusao();
        }
    }//GEN-LAST:event_listModalidadeMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAssociar;
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList listAlunos;
    private javax.swing.JList listModalidade;
    // End of variables declaration//GEN-END:variables
}
