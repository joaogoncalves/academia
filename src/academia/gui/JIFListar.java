package academia.gui;

import academia.modalidade.Modalidade;
import academia.pessoa.Administrador;
import academia.pessoa.Aluno;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Joao
 */
public class JIFListar extends javax.swing.JInternalFrame {

    private String tipo;
    private static String selecionado = "";
    private Aluno aluno;
    private Administrador administrador;

    public JIFListar() {
        initComponents();

        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
    }
    
    public void setListagem(String[] lista, String tipo) {
        adicionaListagem(lista);
        this.tipo = tipo;
    }
    
    public void adicionaListagem(final String[] lista) {
        jList1.setModel(new javax.swing.AbstractListModel() {
            @Override
            public int getSize() {
                return lista.length;
            }

            @Override
            public Object getElementAt(int i) {
                return lista[i];
            }
        });
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2,
                (d.height - this.getSize().height) / 2);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        buttonConfirmar = new javax.swing.JButton();
        buttonCancelar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Listagem");

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Listagem de cadastrados"));

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Vazio" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jList1);

        buttonConfirmar.setText("Confirmar");
        buttonConfirmar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonConfirmarMouseClicked(evt);
            }
        });

        buttonCancelar.setText("Cancelar");
        buttonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonCancelarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(414, Short.MAX_VALUE)
                .addComponent(buttonConfirmar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonCancelar)
                .addGap(22, 22, 22))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonCancelar, buttonConfirmar});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonConfirmar)
                    .addComponent(buttonCancelar))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonConfirmarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonConfirmarMouseClicked
        try {
            selecionado = jList1.getSelectedValue().toString();
            switch (tipo) {
                case "clienteDeletar":
                    JFAplicacao.deletarAluno(selecionado);
                    JOptionPane.showMessageDialog(rootPane, "Aluno Apagado com Sucesso!", "Exclusão", 1);
                    break;
                case "clienteEditar":
                    aluno = JFAplicacao.pesquisarAluno(selecionado);
                    JFAplicacao.frameEditarAluno = new JIFEditarAluno();
                    JFAplicacao.getDesktopPanel().add(JFAplicacao.frameEditarAluno);
                    JFAplicacao.frameEditarAluno.setDadosCliente(aluno);
                    JFAplicacao.frameEditarAluno.setPosicao();
                    JFAplicacao.frameEditarAluno.setVisible(true);
                    
                    break;
                case "modalidadeDeletar":
                    JFAplicacao.deletarModalidade(selecionado);
                    JOptionPane.showMessageDialog(rootPane, "Modalidade Apagada com Sucesso!", "Exclusão", 1);
                    break; 
                    
                case "modalidadeEditar":
                    Modalidade modalidade = JFAplicacao.pesquisaModalidade(selecionado);
                    JFAplicacao.frameEditarModalidade = new JIFEditarModalidade();
                    JFAplicacao.getDesktopPanel().add(JFAplicacao.frameEditarModalidade);
                    JFAplicacao.frameEditarModalidade.setDadosModalidade(modalidade);
                    JFAplicacao.frameEditarModalidade.setPosicao();
                    JFAplicacao.frameEditarModalidade.setVisible(true);
                    
                    break;
                case "loginCliente":
                    aluno = JFAplicacao.pesquisarAluno(selecionado);
                    JFAplicacao.setUsuarioLogado(aluno);
                    break;
                case "loginGestor":
                    administrador = JFAplicacao.pesquisarGestor(selecionado);
                    JFAplicacao.setUsuarioLogado(administrador);
                    break;
            }
            
        } catch (Exception e) {
        }
        
        this.dispose();
    }//GEN-LAST:event_buttonConfirmarMouseClicked

    private void buttonCancelarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonCancelarMouseClicked
        selecionado = "";
        this.dispose();
    }//GEN-LAST:event_buttonCancelarMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JButton buttonConfirmar;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
