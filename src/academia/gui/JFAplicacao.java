package academia.gui;

import academia.cruds.CrudAdministrador;
import academia.cruds.CrudAluno;
import academia.cruds.CrudAlunoModalidade;
import academia.cruds.CrudModalidade;
import academia.modalidade.Modalidade;
import academia.negocio.AcessoGestor;
import academia.negocio.Antropometria;
import academia.pessoa.Administrador;
import academia.pessoa.Aluno;
import academia.pessoa.Pessoa;
import java.awt.Color;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public final class JFAplicacao extends javax.swing.JFrame {

    // Janelas das operações
    private JIFSobre frameSobre;
    private JIFRelatorioAntropometrico frameRelatorioAntropometrico;
    private JIFPagamento framePagamento;
    private JIFRelatorioAlunosPorModalidade frameRelatorioAlunosPorModalidade;
    private JIFRelatorioAlunosStatusPagamento frameRelatorioAlunosPagamento;
    private JIFCadastroAluno frameCadastroAluno;
    protected static JIFEditarAluno frameEditarAluno;
    private JIFCadastroModalidade frameCadastroModalidade;
    protected static JIFEditarModalidade frameEditarModalidade;
    private static JIFCadastrarAlunoModalidade frameAssociarAlunoModalidade;
    private JIFListar frameListar;
    // Usuário logado
    private static Aluno alunoLogado = null;
    private static Administrador administradorLogado = null;
    // Declarações dos tipos
    private static Aluno aluno;
    private static Administrador administrador;
    private static Modalidade modalidade;
    // Gestão
    private static AcessoGestor acessoGestor = new AcessoGestor();
    // CRUDs
    private static CrudAluno crudAluno = new CrudAluno();
    private static CrudAdministrador crudAdministrador = new CrudAdministrador();
    private static CrudModalidade crudModalidade = new CrudModalidade();
    private static CrudAlunoModalidade crudAlunoModalidade = new CrudAlunoModalidade();

    public JFAplicacao() {
        initComponents();
        setPosicao();
        dadosTeste();
    }

    public void dadosTeste() {

        Aluno a = new Aluno("Jose", "09052515477", "Rua X", 'M', 20);
        Aluno b = new Aluno("Fernando", "08052515417", "Rua B", 'M', 20);
        Aluno c = new Aluno("Maria", "09052515474", "Rua A", 'M', 20);

        Antropometria antropometria = new Antropometria(1.70, 65., 20., 120.);
        c.setAntropometria(antropometria);

        cadastrarAluno(a);
        cadastrarAluno(b);
        cadastrarAluno(c);

        Modalidade m1 = new Modalidade("Natação", 001);
        Modalidade m2 = new Modalidade("Musculação", 002);

        crudModalidade.inserir(m1);
        crudModalidade.inserir(m2);

        crudAlunoModalidade.inserir(m1, a);
        crudAlunoModalidade.inserir(m2, b);
        crudAlunoModalidade.deletar(m2, b);
    }

    public static void setUsuarioLogado(Pessoa p) {
        if (p instanceof Administrador) {
            administradorLogado = (Administrador) p;
            setPanelStatus(administradorLogado.toString());
        } else {
            alunoLogado = (Aluno) p;
            setPanelStatus(alunoLogado.getNome());
        }
    }

    public static void setPanelStatus(String usuario) {
        labelStatus.setText("Seja bem-vindo, " + usuario);
    }

    public static void setAlunoLogadoStatusPagamento(boolean status) {
        alunoLogado.setPagamento(status);
    }

    public static JDesktopPane getDesktopPanel() {
        return jDesktopPane1;
    }

    public static Administrador pesquisarGestor(String nome) {
        return null;
    }

    public static void cadastrarAluno(Aluno a) {
        crudAluno.inserir(a);
    }

    public static void editarAluno(Aluno a, String nome, String cpf, String endereco, char sexo, int idade) {
        crudAluno.alterar(a, nome, cpf, endereco, sexo, idade);
    }

    public static Aluno pesquisarAluno(String pesquisa) {
        return crudAluno.consultar(pesquisa);
    }

    public static void deletarAluno(String pesquisa) {
        aluno = crudAluno.consultar(pesquisa);
        crudAluno.deletar(aluno);
    }

    public static void cadastrarModalidade(Modalidade m) {
        crudModalidade.inserir(m);
    }

    public static void editarModalidade(Modalidade m, String nome, int codigo) {
        crudModalidade.alterar(m, nome, codigo);
    }

    public static Modalidade pesquisaModalidade(String pesquisa) {
        return crudModalidade.consultar(pesquisa);
    }

    public static void deletarModalidade(String pesquisa) {
        modalidade = crudModalidade.consultar(pesquisa);
        crudModalidade.deletar(modalidade);
    }

    public static void cadastrarAlunoModalidade(Modalidade m, Aluno a) {
        crudAlunoModalidade.inserir(m, a);
    }

    public static void deletarAlunoModalidade(Modalidade m, Aluno a) {
        crudAlunoModalidade.deletar(m, a);
    }

    public static String[] pesquisarAlunoModalidade(Modalidade m) {
        return acessoGestor.getListaAlunosModalidade(m);
    }

    public static void cadastrarAdministrador(Administrador a) {
        crudAdministrador.inserir(a);
    }

    public static void editarAdministrador(Administrador a, String nome, String cpf) {
        crudAdministrador.alterar(a, nome, cpf);
    }

    public static Administrador pesquisarAdministrador(String pesquisa) {
        return crudAdministrador.consultar(pesquisa);
    }

    public static void deletarAdministrador(Administrador a) {
        crudAdministrador.deletar(a);
    }

    public static String[] getListaAlunosModalidade(String pesquisa) {
        modalidade = crudModalidade.consultar(pesquisa);
        return acessoGestor.getListaAlunosModalidade(modalidade);
    }

    public static String[] getListaAlunos() {
        return acessoGestor.getListaAlunos(crudAluno.getListaAlunos());
    }

    public static String[] getListaAlunosStatus(boolean status) {
        List<Aluno> alunos = crudAluno.getListaAlunos();
        return acessoGestor.getListaAlunosStatus(alunos, status);
    }

    /**
     * Pega as dimensões da tela e posiciona o programa no meio
     */
    public void setPosicao() {
        this.setLocation(((Toolkit.getDefaultToolkit().getScreenSize().width / 2) - (this.getWidth() / 2)),
                ((Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (this.getHeight() / 2)));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        panelStatus = new javax.swing.JPanel();
        labelStatus = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        menuAdministradorSistema = new javax.swing.JMenuItem();
        menuPerfilCliente = new javax.swing.JMenuItem();
        menuPerfilGestor = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        menuAssociarAlunoModalidade = new javax.swing.JMenuItem();
        menuExcluirAlunoModalidade = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenu8 = new javax.swing.JMenu();
        menuGestorCadastrar = new javax.swing.JMenuItem();
        menuGestorEditar = new javax.swing.JMenuItem();
        menuGestorExcluir = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        menuListarAlunosModalidade = new javax.swing.JMenuItem();
        menuListarAlunosStatusPagamento = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuPagamentoCliente = new javax.swing.JMenuItem();
        menuRelatorioAntropometrico = new javax.swing.JMenuItem();
        menuSobre = new javax.swing.JMenu();

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Projeto Academia");

        panelStatus.setBorder(javax.swing.BorderFactory.createCompoundBorder());

        labelStatus.setText("Sem perfil logado");

        javax.swing.GroupLayout panelStatusLayout = new javax.swing.GroupLayout(panelStatus);
        panelStatus.setLayout(panelStatusLayout);
        panelStatusLayout.setHorizontalGroup(
            panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelStatusLayout.setVerticalGroup(
            panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jMenu3.setText("Login");
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });

        menuAdministradorSistema.setText("Administrador");
        menuAdministradorSistema.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAdministradorSistemaActionPerformed(evt);
            }
        });
        jMenu3.add(menuAdministradorSistema);

        menuPerfilCliente.setText("Perfil Cliente");
        menuPerfilCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPerfilClienteActionPerformed(evt);
            }
        });
        jMenu3.add(menuPerfilCliente);

        menuPerfilGestor.setText("Perfil Gestor");
        menuPerfilGestor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPerfilGestorActionPerformed(evt);
            }
        });
        jMenu3.add(menuPerfilGestor);

        jMenuItem5.setText("Sair do Perfil");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        jMenuBar1.add(jMenu3);

        jMenu1.setText("Menu Gestor");

        jMenu6.setText("Aluno");

        jMenuItem9.setText("Cadastrar");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem9);

        jMenuItem6.setText("Editar");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem6);

        jMenuItem2.setText("Excluir");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem2);

        jMenu1.add(jMenu6);

        jMenu5.setText("Modalidade");

        jMenuItem12.setText("Cadastrar");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem12);

        jMenuItem13.setText("Editar");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem13);

        jMenuItem14.setText("Excluir");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem14);

        jMenu1.add(jMenu5);

        jMenu7.setText("Aluno em Modalidade");

        menuAssociarAlunoModalidade.setText("Associar");
        menuAssociarAlunoModalidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAssociarAlunoModalidadeActionPerformed(evt);
            }
        });
        jMenu7.add(menuAssociarAlunoModalidade);

        menuExcluirAlunoModalidade.setText("Excluir");
        menuExcluirAlunoModalidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExcluirAlunoModalidadeActionPerformed(evt);
            }
        });
        jMenu7.add(menuExcluirAlunoModalidade);

        jMenu1.add(jMenu7);
        jMenu1.add(jSeparator1);

        jMenu8.setText("Administrador");
        jMenu8.setEnabled(false);

        menuGestorCadastrar.setText("Cadastrar");
        menuGestorCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGestorCadastrarActionPerformed(evt);
            }
        });
        jMenu8.add(menuGestorCadastrar);

        menuGestorEditar.setText("Editar");
        jMenu8.add(menuGestorEditar);

        menuGestorExcluir.setText("Excluir");
        jMenu8.add(menuGestorExcluir);

        jMenu1.add(jMenu8);

        jMenu4.setText("Relatórios");

        menuListarAlunosModalidade.setText("Alunos por Modalidade");
        menuListarAlunosModalidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuListarAlunosModalidadeActionPerformed(evt);
            }
        });
        jMenu4.add(menuListarAlunosModalidade);

        menuListarAlunosStatusPagamento.setText("Alunos por Status de Pagamento");
        menuListarAlunosStatusPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuListarAlunosStatusPagamentoActionPerformed(evt);
            }
        });
        jMenu4.add(menuListarAlunosStatusPagamento);

        jMenu1.add(jMenu4);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Menu Cliente");

        menuPagamentoCliente.setText("Pagamento");
        menuPagamentoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPagamentoClienteActionPerformed(evt);
            }
        });
        jMenu2.add(menuPagamentoCliente);

        menuRelatorioAntropometrico.setText("Relatório Antropométrico");
        menuRelatorioAntropometrico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRelatorioAntropometricoActionPerformed(evt);
            }
        });
        jMenu2.add(menuRelatorioAntropometrico);

        jMenuBar1.add(jMenu2);

        menuSobre.setText("Sobre");
        menuSobre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuSobreMouseClicked(evt);
            }
        });
        jMenuBar1.add(menuSobre);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
            .addComponent(panelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(panelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuPerfilClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPerfilClienteActionPerformed

        if ((frameListar == null) || (!frameListar.isVisible())) {
            frameListar = new JIFListar();
            jDesktopPane1.add(frameListar);
            frameListar.setPosicao();

            // Adicionando os itens
            List<Aluno> alunos = crudAluno.getListaAlunos();
            String[] itensNomes = acessoGestor.getListaAlunos(alunos);

            frameListar.setListagem(itensNomes, "loginCliente");
            frameListar.setVisible(true);
        }
    }//GEN-LAST:event_menuPerfilClienteActionPerformed

    private void menuSobreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuSobreMouseClicked
        if ((frameSobre == null) || (!frameSobre.isVisible())) {
            frameSobre = new JIFSobre();
            jDesktopPane1.add(frameSobre);
            frameSobre.setPosicao();
            frameSobre.setVisible(true);
        }
    }//GEN-LAST:event_menuSobreMouseClicked

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        if ((frameListar == null) || (!frameListar.isVisible())) {
            try {

                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameListar = new JIFListar();
                    jDesktopPane1.add(frameListar);
                    frameListar.setPosicao();

                    // Adicionando os itens
                    List<Aluno> alunos = crudAluno.getListaAlunos();
                    String[] itensNomes = acessoGestor.getListaAlunos(alunos);

                    frameListar.setListagem(itensNomes, "clienteDeletar");
                    frameListar.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void menuRelatorioAntropometricoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRelatorioAntropometricoActionPerformed

        if ((frameRelatorioAntropometrico == null) || (!frameRelatorioAntropometrico.isVisible())) {

            try {
                /**
                 * Se não houver aluno logado, exibe uma aviso de erro
                 */
                if (alunoLogado == null) {
                    throw new RuntimeException();

                } else {
                    frameRelatorioAntropometrico = new JIFRelatorioAntropometrico();
                    jDesktopPane1.add(frameRelatorioAntropometrico);
                    frameRelatorioAntropometrico.setPosicao();
                    frameRelatorioAntropometrico.setVisible(true);

                    frameRelatorioAntropometrico.setDadosCliente(alunoLogado);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há cliente logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_menuRelatorioAntropometricoActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed

        if ((frameCadastroAluno == null) || (!frameCadastroAluno.isVisible())) {
            try {
                /**
                 * Se não houver administrador logado, exibe uma aviso de erro
                 */
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameCadastroAluno = new JIFCadastroAluno();
                    jDesktopPane1.add(frameCadastroAluno);
                    frameCadastroAluno.setPosicao();
                    frameCadastroAluno.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed

        alunoLogado = null;
        administradorLogado = null;
        labelStatus.setText("Sem perfil logado");
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void menuGestorCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuGestorCadastrarActionPerformed
        //if (() )
    }//GEN-LAST:event_menuGestorCadastrarActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed

        if ((frameCadastroModalidade == null) || (!frameCadastroModalidade.isVisible())) {
            try {
                /**
                 * Se não houver administrador logado, exibe uma aviso de erro
                 */
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameCadastroModalidade = new JIFCadastroModalidade();
                    jDesktopPane1.add(frameCadastroModalidade);
                    frameCadastroModalidade.setPosicao();
                    frameCadastroModalidade.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed

        if ((frameListar == null) || (!frameListar.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameListar = new JIFListar();
                    jDesktopPane1.add(frameListar);
                    frameListar.setPosicao();

                    // Adicionando os itens
                    List<Aluno> alunos = crudAluno.getListaAlunos();
                    String[] itensNomes = acessoGestor.getListaAlunos(alunos);

                    frameListar.setListagem(itensNomes, "clienteEditar");
                    frameListar.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        if ((frameListar == null) || (!frameListar.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameListar = new JIFListar();
                    jDesktopPane1.add(frameListar);
                    frameListar.setPosicao();

                    // Adicionando os itens
                    List<Modalidade> modalidades = crudModalidade.getListaModalidades();
                    String[] itensNomes = acessoGestor.getListaModalidades(modalidades);

                    frameListar.setListagem(itensNomes, "modalidadeEditar");
                    frameListar.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed

        if ((frameListar == null) || (!frameListar.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameListar = new JIFListar();
                    jDesktopPane1.add(frameListar);
                    frameListar.setPosicao();

                    // Adicionando os itens
                    List<Modalidade> modalidades = crudModalidade.getListaModalidades();
                    String[] itensNomes = acessoGestor.getListaModalidades(modalidades);

                    frameListar.setListagem(itensNomes, "modalidadeDeletar");
                    frameListar.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void menuPagamentoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPagamentoClienteActionPerformed
        if ((framePagamento == null) || (!framePagamento.isVisible())) {
            try {
                if (alunoLogado == null) {
                    throw new RuntimeException();
                } else {
                    framePagamento = new JIFPagamento();
                    jDesktopPane1.add(framePagamento);
                    framePagamento.setPosicao();

                    framePagamento.setStatusPagamento(alunoLogado);
                    framePagamento.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há cliente logado!", "Aviso", 0);
            }

        }
    }//GEN-LAST:event_menuPagamentoClienteActionPerformed

    private void menuAdministradorSistemaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAdministradorSistemaActionPerformed
        administrador = new Administrador("Administrador do Sistema");
        setUsuarioLogado(administrador);
    }//GEN-LAST:event_menuAdministradorSistemaActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
        if ((frameListar == null) || (!frameListar.isVisible())) {
            frameListar = new JIFListar();
        }
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void menuPerfilGestorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPerfilGestorActionPerformed
        if ((frameListar == null) || (!frameListar.isVisible())) {
            frameListar = new JIFListar();
            jDesktopPane1.add(frameListar);
            frameListar.setPosicao();

            // Adicionando os itens
            List<Administrador> administradores = crudAdministrador.getListaAdministradores();
            String[] itensNomes = acessoGestor.getListaAdministradores(administradores);

            frameListar.setListagem(itensNomes, "loginGestor");
            frameListar.setVisible(true);
        }
    }//GEN-LAST:event_menuPerfilGestorActionPerformed

    private void menuAssociarAlunoModalidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAssociarAlunoModalidadeActionPerformed
        if ((frameAssociarAlunoModalidade == null) || (!frameAssociarAlunoModalidade.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameAssociarAlunoModalidade = new JIFCadastrarAlunoModalidade();
                    jDesktopPane1.add(frameAssociarAlunoModalidade);
                    frameAssociarAlunoModalidade.setPosicao();

                    // Adicionando os itens dos alunos
                    List<Aluno> alunos = crudAluno.getListaAlunos();
                    String[] itensAlunos = acessoGestor.getListaAlunos(alunos);

                    // Adicionando os itens da modalidades
                    List<Modalidade> modalidades = crudModalidade.getListaModalidades();
                    String[] itensModalidade = acessoGestor.getListaModalidades(modalidades);

                    frameAssociarAlunoModalidade.setListagemCadastrar(itensModalidade, itensAlunos);

                    frameAssociarAlunoModalidade.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_menuAssociarAlunoModalidadeActionPerformed

    private void menuExcluirAlunoModalidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExcluirAlunoModalidadeActionPerformed
        if ((frameAssociarAlunoModalidade == null) || (!frameAssociarAlunoModalidade.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameAssociarAlunoModalidade = new JIFCadastrarAlunoModalidade();
                    jDesktopPane1.add(frameAssociarAlunoModalidade);
                    frameAssociarAlunoModalidade.setPosicao();

                    // Adicionando os itens
                    List<Modalidade> modalidades = crudModalidade.getListaModalidades();
                    String[] itensModalidade = acessoGestor.getListaModalidades(modalidades);

                    frameAssociarAlunoModalidade.setListagemExcluir(itensModalidade);
                    frameAssociarAlunoModalidade.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_menuExcluirAlunoModalidadeActionPerformed

    private void menuListarAlunosStatusPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuListarAlunosStatusPagamentoActionPerformed
        if ((frameRelatorioAlunosPagamento == null) || (!frameRelatorioAlunosPagamento.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameRelatorioAlunosPagamento = new JIFRelatorioAlunosStatusPagamento();
                    jDesktopPane1.add(frameRelatorioAlunosPagamento);
                    frameRelatorioAlunosPagamento.setPosicao();

                    frameRelatorioAlunosPagamento.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_menuListarAlunosStatusPagamentoActionPerformed

    private void menuListarAlunosModalidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuListarAlunosModalidadeActionPerformed

        if ((frameRelatorioAlunosPorModalidade == null) || (!frameRelatorioAlunosPorModalidade.isVisible())) {
            try {
                if (administradorLogado == null) {
                    throw new RuntimeException();
                } else {
                    frameRelatorioAlunosPorModalidade = new JIFRelatorioAlunosPorModalidade();
                    jDesktopPane1.add(frameRelatorioAlunosPorModalidade);
                    frameRelatorioAlunosPorModalidade.setPosicao();

                    // Adicionando os itens
                    List<Modalidade> modalidades = crudModalidade.getListaModalidades();
                    String[] itensModalidade = acessoGestor.getListaModalidades(modalidades);

                    frameRelatorioAlunosPorModalidade.adicionarListagemModalidades(itensModalidade);

                    frameRelatorioAlunosPorModalidade.setVisible(true);
                }
            } catch (RuntimeException re) {
                JOptionPane.showMessageDialog(rootPane, "Não há administrador logado!", "Aviso", 0);
            }
        }
    }//GEN-LAST:event_menuListarAlunosModalidadeActionPerformed

    public static void main(String args[]) {
        /**
         * A aplicação utilizada o L&F Nimbus com cores personalizadas
         */
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }

            UIManager.put("nimbusBase", new Color(59, 140, 47));
            UIManager.put("nimbusBlueGrey", new Color(170, 190, 184));
            UIManager.put("control", new Color(214, 223, 220));

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFAplicacao.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new JFAplicacao().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected static javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTextField jTextField1;
    private static javax.swing.JLabel labelStatus;
    private javax.swing.JMenuItem menuAdministradorSistema;
    private javax.swing.JMenuItem menuAssociarAlunoModalidade;
    private javax.swing.JMenuItem menuExcluirAlunoModalidade;
    private javax.swing.JMenuItem menuGestorCadastrar;
    private javax.swing.JMenuItem menuGestorEditar;
    private javax.swing.JMenuItem menuGestorExcluir;
    private javax.swing.JMenuItem menuListarAlunosModalidade;
    private javax.swing.JMenuItem menuListarAlunosStatusPagamento;
    private javax.swing.JMenuItem menuPagamentoCliente;
    private javax.swing.JMenuItem menuPerfilCliente;
    private javax.swing.JMenuItem menuPerfilGestor;
    private javax.swing.JMenuItem menuRelatorioAntropometrico;
    private javax.swing.JMenu menuSobre;
    private javax.swing.JPanel panelStatus;
    // End of variables declaration//GEN-END:variables
}
