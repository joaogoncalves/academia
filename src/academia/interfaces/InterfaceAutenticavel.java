package academia.interfaces;

public interface InterfaceAutenticavel<P> {
    
    public boolean autenticavel(String id, P identificacao);
    
}
