package academia.interfaces;

/**
 * 
 * @param <P> Tipo de objeto passado atráves da Classe que implementa esta interface
 * com a utilização de Generics.
 */
public interface InterfaceCRUD<P> {
    
    public void inserir(P parametro);
    
    public void deletar(P parametro);
    
    public P consultar(String nome);
}
