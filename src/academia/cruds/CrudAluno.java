package academia.cruds;

import academia.interfaces.InterfaceCRUD;
import academia.pessoa.Aluno;
import java.util.ArrayList;
import java.util.List;

public final class CrudAluno implements InterfaceCRUD<Aluno> {

    private List<Aluno> listaAlunos;

    public CrudAluno() {
        listaAlunos = new ArrayList<>();
    }

    @Override
    public void inserir(Aluno a) {
        /**
         * Coloca a matricula automaticamente para cada usuário criado
         */
        a.setMatricula( listaAlunos.size() + 1);
        listaAlunos.add(a);
    }

    @Override
    public void deletar(Aluno a) {
        listaAlunos.remove(a);
        
        for (int i = 0; i < listaAlunos.size(); i++) {
            Aluno aluno = listaAlunos.get(i);
            aluno.setMatricula(i + 1);
            
        }
    }

    public void alterar(Aluno a, String nome, String cpf, String endereco, char sexo, int idade) {

        a.setNome(nome);
        a.setCpf(cpf);
        a.setEndereco(endereco);
        a.setSexo(sexo);
        a.setIdade(idade);
    }

    @Override
    public Aluno consultar(String pesquisa) {

        for (Aluno aluno : listaAlunos) {
            if (aluno.toString().equals(pesquisa)) {
                return aluno;
            }
        }
        return null;
    }

    public List<Aluno> getListaAlunos() {
        return listaAlunos;
    }

    public void setListaAlunos(List<Aluno> listaAlunos) {
        this.listaAlunos = listaAlunos;
    }
}