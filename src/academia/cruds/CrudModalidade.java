package academia.cruds;

import academia.interfaces.InterfaceCRUD;
import academia.modalidade.Modalidade;
import java.util.LinkedList;
import java.util.List;

public final class CrudModalidade implements InterfaceCRUD<Modalidade> {
    
    private List<Modalidade> listaModalidades;

    public CrudModalidade() {
        listaModalidades = new LinkedList<>();
    }
    
    @Override
    public void inserir(Modalidade m) {
        listaModalidades.add(m);
    }
    
    @Override
    public void deletar(Modalidade m) {
        listaModalidades.remove(m);
    }
    
    public void alterar(Modalidade modalidade, String nome, int codigo) {
        modalidade.setNome(nome);
        modalidade.setCodigo(codigo);
    }
    
    @Override
    public Modalidade consultar(String pesquisa) {
        
        for (Modalidade m : listaModalidades) {
            if (m.toString().equals(pesquisa)) {
                return m;
            }
        }
        return null;
    }

    public List<Modalidade> getListaModalidades() {
        return listaModalidades;
    }

    public void setListaModalidades(List<Modalidade> listaModalidades) {
        this.listaModalidades = listaModalidades;
    }
}