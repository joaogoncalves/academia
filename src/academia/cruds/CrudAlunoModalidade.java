package academia.cruds;

import academia.modalidade.Modalidade;
import academia.pessoa.Aluno;
import java.util.List;

public final class CrudAlunoModalidade {
    
    public void inserir(Modalidade modalidade, Aluno aluno) {
        modalidade.adicionar(aluno);
    }
       
    public void deletar(Modalidade modalidade, Aluno aluno) {
        modalidade.remover(aluno);
    }
       
    public List<Aluno> consultarAlunoPorModalidade(Modalidade modalidade) {
        
        return modalidade.getModalidadeAlunos();
    }
}
