package academia.cruds;

import academia.pessoa.Administrador;
import java.util.ArrayList;
import java.util.List;

public final class CrudAdministrador {

    private List<Administrador> listaAdministradores;

    public CrudAdministrador() {
        listaAdministradores = new ArrayList<>();
    }

    public void inserir(Administrador a) {
        /**
         * Coloca a matricula automaticamente para cada usuário criado
         */
        a.setMatricula( listaAdministradores.size() + 1);
        listaAdministradores.add(a);
    }

    public void deletar(Administrador a) {
        listaAdministradores.remove(a);
        
        for (int i = 0; i < listaAdministradores.size(); i++) {
            Administrador administrador = listaAdministradores.get(i);
            administrador.setMatricula(i + 1);
            
        }
    }

    public void alterar(Administrador a, String nome, String cpf) {

        a.setNome(nome);
        a.setCpf(cpf);
    }

    public Administrador consultar(String pesquisa) {

        for (Administrador administrador : listaAdministradores) {
            if (administrador.toString().equals(pesquisa)) {
                return administrador;
            }
        }
        return null;
    }

    public List<Administrador> getListaAdministradores() {
        return listaAdministradores;
    }

    public void setListaAdministradores(List<Administrador> listaAdministradores) {
        this.listaAdministradores = listaAdministradores;
    }
}